

#vpc

#data "aws_vpc" "wordpress" {

#  filter {
#    name = "vpc-id"
#    values = ["vpc-07185c9da773f72b9"]
#        }
        
#}

#networking

resource "aws_subnet" "subnet_1" {
  vpc_id            = "${data.aws_vpc.selected.id}"
  cidr_block        = "${var.subnet_1_cidr}"
  availability_zone = "${var.az_1}"

  tags = {
    Name = "main_subnet1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = "${data.aws_vpc.selected.id}"
  cidr_block        = "${var.subnet_2_cidr}"
  availability_zone = "${var.az_2}"

  tags = {
    Name = "main_subnet2"
  }
}
#######
/*
resource "aws_internet_gateway" "gw" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags = {
    Name = "main igw"
  }
}

resource "aws_route_table" "r" {
  vpc_id = "${data.aws_vpc.selected.id}"

  route {
    cidr_block = "10.0.1.0/24"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
tags = {
    Name = "main routetable"
  }

}


resource "aws_route" "public_internet_gateway"  {
  route_table_id            = "aws_route_table.public[0].id"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = "igw-00156963278cf6cdc"

}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route.public_internet_gateway.id
}
*/
#########


resource "aws_security_group" "default" {
  name        = "main_rds_sg"
  description = "Allow all inbound traffic"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    cidr_blocks = ["${var.cidr_blocks}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.sg_name}"
  }
}



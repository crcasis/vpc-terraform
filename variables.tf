variable "vpc_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)
  default     = ["sg-0c396159788e86cab"]
}
#variable "security_groups" {}

#data "aws_security_group" "terraform" {
#  id = "${var.security_groups}"
#}

variable "subnet_ec2" {
  description = "The VPC Subnet ID to launch in"
  type        = string
  default     = "subnet-099bf2e9ad1ae4c4a"
}

variable "subnet_rds" {
  description = "The VPC Subnet ID to launch in"
  type        = string
  default     = "rds-wordpress"
}

variable "vpc_id" {
  description = "Your VPC ID"
  type        = string
  default = "vpc-0d36c2d5b18e85f05"
}
data "aws_vpc" "selected" {
  id = "${var.vpc_id}"
}

variable "aws_env_name" {
  default = "dev"
}
